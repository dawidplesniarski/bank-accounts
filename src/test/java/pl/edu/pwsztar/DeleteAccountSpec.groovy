package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification{

    @Unroll
    def "Return balance of deleted account" (){
        given: "initial data"
            Bank bank = new Bank()
            int accountNo = bank.createAccount()
            bank.deposit(accountNo, amount)
        when: "We delete account"
            int balanceOfDelAccount = bank.deleteAccount(accountNo)
        then: "We should get balance of deleted account"
            balanceOfDelAccount == balance
        where:
            amount  ||  balance
            300     ||  300
            25      ||  25

    }

    def "Ability to delete account with no balance" (){
        given: "initial data"
            Bank bank = new Bank()
            int accountNo = bank.createAccount()
        when: "try to delete account"
            int balanceOfDeletedAccount = bank.deleteAccount(accountNo)
        then: "balance should equals 0"
            balanceOfDeletedAccount == 0
    }

    def "Not able to delete account if not exists" (){
        given: "Initial data"
            Bank bank = new Bank()
        when: "We try to delete account that not exists"
            int balanceOfDelAccount = bank.deleteAccount(1)
        then: "Delete of account failed"
            balanceOfDelAccount == BankOperation.ACCOUNT_NOT_EXISTS
    }
}

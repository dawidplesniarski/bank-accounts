package pl.edu.pwsztar

import spock.lang.Specification

class DepositSpec extends Specification{

    def "able to deposit money for existing account" () {
        given: "initial data"
            Bank bank = new Bank()
            int accountNo = bank.createAccount()
        when: "deposit money"
            boolean ableToDeposit = bank.deposit(accountNo, 2500)
        then: "deposit successful"
            ableToDeposit
    }

    def "can not deposit money for account that not exists" (){
        given: "initial data"
            Bank bank = new Bank()
        when: "we try to deposit for not existing account"
            boolean ableToDeposit = bank.deposit(1,2000)
        then: "Failed to deposit"
            !ableToDeposit
    }
}

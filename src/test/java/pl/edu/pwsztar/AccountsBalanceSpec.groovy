package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountsBalanceSpec extends Specification {

    @Unroll
    def "return balance of specific account" (){
        given: "Existing account"
            Bank bank = new Bank();
            int accountNo = bank.createAccount()
            bank.deposit(accountNo, balance)
        when: "Expect to get specific account balance"
            Double accountBalance = bank.accountBalance(accountNo)
        then: "balance should equals: #balance"
            accountBalance == balance
        where:
        amount  ||  balance
        150     ||  150
        325     ||  325
    }

    def "If account not exists we won't get balance" () {
        given:
            Bank bank = new Bank();
        when: "We try to get account balance"
        int accountBalance = bank.accountBalance(1)
        then: "Account not exists"
        accountBalance == BankOperation.ACCOUNT_NOT_EXISTS
    }
}

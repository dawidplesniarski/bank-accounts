package pl.edu.pwsztar

import spock.lang.Specification

class TransferSpec extends Specification {

    def "Able to tranfer money if amount <= balance" (){
        given: "initial data"
            Bank bank = new Bank()
            int firstAccount = bank.createAccount()
            int secondAccount = bank.createAccount()
            bank.deposit(firstAccount,500)
        when: "We try to transfer money from first to second account"
            boolean ableToTransfer = bank.transfer(firstAccount, secondAccount, 450)
        then: "Transfer complete"
            ableToTransfer
    }

    def "Not able to transfer money if balance < amount" (){
        given: "initial data"
            Bank bank = new Bank()
            int firstAccount = bank.createAccount()
            int secondAccount = bank.createAccount()
            bank.deposit(firstAccount,500)
        when: "We try to transfer more than we have"
            boolean ableToTransfer = bank.transfer(firstAccount, secondAccount, 650)
        then: "Transfer failed"
            !ableToTransfer
    }
}

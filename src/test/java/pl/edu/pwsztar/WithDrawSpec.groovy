package pl.edu.pwsztar

import spock.lang.Specification

class WithDrawSpec extends  Specification{

    def "If account exists and have enough balance we can withdraw money" (){
        given: "Initial data"
            Bank bank = new Bank()
            int account = bank.createAccount()
            bank.deposit(account, 500)
        when: "We try to withdraw money"
            boolean ableToWithdraw = bank.withdraw(account,400)
        then: "Withdraw operation successful"
            ableToWithdraw
    }

    def "We cannot withdraw money when account not exists" (){
        given: "initial data"
            Bank bank = new Bank()
        when: "try to withdraw with no account"
            boolean ableToWithdraw = bank.withdraw(1,300)
        then:
            !ableToWithdraw
    }

    def "If account have not enough balance we cannot withdraw" (){
        given: "Initial data"
        Bank bank = new Bank()
        int account = bank.createAccount()
        bank.deposit(account, 500)
        when: "We try to withdraw money"
        boolean ableToWithdraw = bank.withdraw(account,600)
        then: "Withdraw operation failed"
        !ableToWithdraw
    }


}

package pl.edu.pwsztar

import spock.lang.Specification

class AccountSumSpec extends Specification{

    def "return sum of all accounts balance" (){
        given: "initial data"
            Bank bank = new Bank()
            int first = bank.createAccount()
            int second = bank.createAccount()
            int third = bank.createAccount()
            bank.deposit(first,500)
            bank.deposit(second,1500)
            bank.deposit(third,300)
        when: "expect to get sum of all accounts balance"
            int sum = bank.sumAccountsBalance()
        then: "expect to get sum balance"
            sum == 2300
    }
}

package pl.edu.pwsztar;

import java.util.List;

public interface AccountService {
    Account getAccountByNumber(int accountNumber);
    Account saveAccount(Account account);
    void deleteAccount(int accountNumber);
    List<Account> findAllAccounts();
}

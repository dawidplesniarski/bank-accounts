package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private final AccountService accountService;
    private static int accountNumber = 0;
    private static final int EMPTY_ACCOUNT = 0;

    public Bank() {
        this.accountService = new AccountsStorage();
    }

    public int createAccount() {
        ++accountNumber;
        accountService.saveAccount(new Account(accountNumber, EMPTY_ACCOUNT));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        final int saldo = accountBalance(accountNumber);
        if(saldo == ACCOUNT_NOT_EXISTS){
            System.out.println("Account with number "+accountNumber+" not exists");
        }else{
            accountService.deleteAccount(accountNumber);
        }
        return saldo;
    }

    public Account getAccount(int accountNumber){
        return accountService.getAccountByNumber(accountNumber);
    }

    public boolean deposit(int accountNumber, int amount) {
        final Account account = getAccount(accountNumber);
        if (Optional.ofNullable(account).isPresent()) {
            account.accountDeposit(amount);
            accountService.saveAccount(account);
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = Optional.ofNullable(accountService.getAccountByNumber(accountNumber));
        return account.isPresent() && account.get().getAccountBalance() >= amount;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Optional<Account> account = Optional.ofNullable(accountService.getAccountByNumber(fromAccount));
        return account.get().getAccountBalance() >= amount;
    }

    public int accountBalance(int accountNumber) {
        final Optional<Account> account = Optional.ofNullable(getAccount(accountNumber));
        return account.map(Account::getAccountBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {
        int sum = 0;
        for(Account account : accountService.findAllAccounts()){
            sum += account.getAccountBalance();
        }
        return sum;
    }
}
